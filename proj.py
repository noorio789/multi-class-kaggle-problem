import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import os
import gc

import keras as k
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
import cv2
from tqdm import tqdm
import sys
from pathlib import Path


from keras.applications.xception import Xception
from keras.applications.xception import preprocess_input
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model, Sequential
from keras.layers import Dense, GlobalAveragePooling2D, Dropout, Activation, Flatten
from keras.callbacks import ModelCheckpoint, TensorBoard, ReduceLROnPlateau

from keras.layers.advanced_activations import LeakyReLU, ELU, PReLU, ThresholdedReLU
from keras.activations import relu, tanh, elu
from keras.utils import np_utils
from keras.layers.normalization import BatchNormalization
from keras import regularizers
from multiprocessing import Pool
from scipy.misc import imresize
import numpy as np
import os
import keras.backend as K
from keras.callbacks import Callback

class SGDLearningRateTracker(Callback):
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        lr = K.eval(optimizer.lr * (1. / (1. + optimizer.decay * optimizer.iterations)))
        print('\nLR: {:.6f}\n'.format(lr))



# variable defs
run_num = 4
bat_size = 16
rand_seed = 1337
epoch_nums = 20
# train_image_num = 80000
# validate_image_num = 20000
# test_image_num = 10000
# image_size = 71
# rescale = 1. / 255
# shift = 0.1

local_path = str(Path(os.getcwd()).parent)+'/'
run = "run" + str(run_num) + "/"
saved_data_folder_path = local_path+"runs_saved_data/"
weights_path = "weights/weights"
graphs_path = "graphs/"
sub_path = "submission/"


weights_file_path = saved_data_folder_path + run + weights_path
log_dir_path = saved_data_folder_path + run + graphs_path
submission_path = saved_data_folder_path + run + sub_path


if not os.path.exists(saved_data_folder_path):
    os.makedirs(saved_data_folder_path)
if not os.path.exists(log_dir_path):
    os.makedirs(log_dir_path)
if not os.path.exists(submission_path):
    os.makedirs(submission_path)
if not os.path.exists(weights_file_path):
    os.makedirs(weights_file_path)





sys.path.append('/usr/local/lib/python3.5/site-packages')

x_train = []
x_test = []
y_train = []

df_train = pd.read_csv('/home/ahmednofal/Downloads/train.csv/train.csv')
df_test = pd.read_csv('/home/ahmednofal/Downloads/sample_submission.csv/sample_submission.csv')

flatten = lambda l: [item for sublist in l for item in sublist]
labels = list(set(flatten([l.split(' ') for l in df_train['tags'].values])))

label_map = {l: i for i, l in enumerate(labels)}
inv_label_map = {i: l for l, i in label_map.items()}

for f, tags in tqdm(df_train.values, miniters=1000):
    img = cv2.imread('/home/ahmednofal/Documents/project/train-jpg/{}.jpg'.format(f))
    targets = np.zeros(17)
    for t in tags.split(' '):
        targets[label_map[t]] = 1
    x_train.append(cv2.resize(img, (32, 32)))
    y_train.append(targets)

for f, tags in tqdm(df_test.values, miniters=1000):
    img = cv2.imread('/home/ahmednofal/Documents/project/test-jpg/{}.jpg'.format(f))
    x_test.append(cv2.resize(img, (32, 32)))

y_train = np.array(y_train, np.uint8)
x_train = np.array(x_train, np.float16) / 255.
x_test  = np.array(x_test, np.float32) / 255.


print(x_train.shape)
print(y_train.shape)

split = 35000
x_train, x_valid, y_train, y_valid = x_train[:split], x_train[split:], y_train[:split], y_train[split:]
#
# model = Sequential()
# model.add(Conv2D(32, kernel_size=(3, 3),
#                  activation='relu',
#                  input_shape=(32, 32, 3)))
#
# model.add(Conv2D(64, (3, 3), activation='relu'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
# model.add(Dropout(0.25))
# model.add(Flatten())
# model.add(Dense(128, activation='relu'))
# model.add(Dropout(0.5))
# model.add(Dense(17, activation='sigmoid'))
#

base_model = Xception(weights='imagenet', include_top=False)
x = base_model.output
x = GlobalAveragePooling2D()(x)
x = Dropout(0.3)(x)
predictions = Dense(17, activation='sigmoid')(x)
model = Model(
    input=base_model.input,
    output=predictions)

checkpointer = ModelCheckpoint(
    filepath=weights_file_path + "_{epoch:02d}_{acc:.4f}_{loss:.4f}_{val_acc:.4f}_{val_loss:.4f}.h5",
    save_best_only=True,
    save_weights_only=False,
    monitor='val_loss',
    verbose=0,
    mode='auto',
    period=1)
graph_save = TensorBoard(
    log_dir=log_dir_path,
    histogram_freq=0,
    write_graph=True,
    write_images=False)
rp = ReduceLROnPlateau(monitor='val_loss',
                  factor=0.5,
                  patience=1,
                  verbose=0,
                  mode='auto',
                  epsilon=0.0005,
                  cooldown=1,
                  min_lr=0.0001)

model.compile(loss='binary_crossentropy',
              # We NEED binary here, since categorical_crossentropy l1 norms the output before calculating loss.
              optimizer='adam',
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=128,
          epochs=epoch_nums,
          verbose=1,
          callbacks=[checkpointer, graph_save, SGDLearningRateTracker(), rp],
          validation_data=(x_valid, y_valid))

from sklearn.metrics import fbeta_score

p_valid = model.predict(x_valid, batch_size=128, verbose=2)
print(fbeta_score(y_valid, np.array(p_valid) > 0.2, beta=2, average='samples'))


p_test = model.predict(x_test, batch_size=128, verbose=2)


result = np.array(p_test)

result = pd.DataFrame(result, columns=labels)
print(result)

preds = []
for i in tqdm(range(result.shape[0]), miniters=1000):
    a = result.ix[[i]]
    a = a.apply(lambda x: x > 0.2, axis=1)
    a = a.transpose()
    a = a.loc[a[i] == True]
    ' '.join(list(a.index))
    preds.append(' '.join(list(a.index)))

df_test['tags'] = preds
print(df_test)

df_test.to_csv(submission_path+'submission_keras.csv', index=False)
